package tech.borzenko.dayimpression.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import tech.borzenko.dayimpression.R;
import tech.borzenko.dayimpression.model.Impression;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 26.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class ImpressionAdapter
        extends RecyclerView.Adapter<ImpressionAdapter.ImpressionViewHolder> {
    private List<Impression> impressions;

    private SimpleDateFormat dateFormat;

    public ImpressionAdapter(List<Impression> impressions) {
        this.impressions = impressions;

        dateFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
    }

    @Override
    public ImpressionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_impression, parent, false);
        return new ImpressionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImpressionViewHolder holder, int position) {
        Impression impression = impressions.get(position);

        holder.dateTextView.setText(dateFormat.format(impression.getDate()));
        holder.textTextView.setText(impression.getText());
    }

    @Override
    public int getItemCount() {
        return impressions != null ? impressions.size() : 0;
    }

    public void setImpressions(List<Impression> impressions) {
        this.impressions = impressions;
        notifyDataSetChanged();
    }

    public static class ImpressionViewHolder extends RecyclerView.ViewHolder {
        public TextView dateTextView;
        public TextView textTextView;

        public ImpressionViewHolder(View itemView) {
            super(itemView);

            dateTextView = (TextView) itemView.findViewById(R.id.tv_date);
            textTextView = (TextView) itemView.findViewById(R.id.tv_text);
        }
    }
}
