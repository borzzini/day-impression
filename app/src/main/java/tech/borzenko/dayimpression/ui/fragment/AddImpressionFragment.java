package tech.borzenko.dayimpression.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.Date;

import tech.borzenko.dayimpression.R;
import tech.borzenko.dayimpression.model.Impression;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 26.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class AddImpressionFragment extends Fragment {
    private EditText impressionEditText;

    private OnDataValidityListener onDataValidityListener;
    private OnSaveListener onSaveListener;

    public AddImpressionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_impression, container, false);

        impressionEditText = (EditText) view.findViewById(R.id.et_impression);
        impressionEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Empty
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validate();
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Empty
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnDataValidityListener) {
            onDataValidityListener = (OnDataValidityListener) context;

            if (context instanceof OnSaveListener) {
                onSaveListener = (OnSaveListener) context;
            } else {
                throw new RuntimeException(context.toString()
                        + " must implement " + OnSaveListener.class.getSimpleName());
            }
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement " + OnDataValidityListener.class.getSimpleName());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        onDataValidityListener = null;
        onSaveListener = null;
    }

    private void validate() {
        if (onDataValidityListener != null) {
            boolean valid = false;
            if (impressionEditText.getText().toString().length() != 0) {
                valid = true;
            }
            onDataValidityListener.onDataValid(valid);
        }
    }

    public void saveImpression() {
        Impression impression = new Impression(new Date(), impressionEditText.getText().toString());
        onSaveListener.onSaveImpression(impression);
    }

    public interface OnDataValidityListener {
        void onDataValid(boolean valid);
    }

    public interface OnSaveListener {
        void onSaveImpression(Impression impression);
    }
}
