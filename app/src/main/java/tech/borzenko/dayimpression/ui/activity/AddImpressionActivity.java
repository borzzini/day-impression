package tech.borzenko.dayimpression.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import tech.borzenko.dayimpression.R;
import tech.borzenko.dayimpression.model.Impression;
import tech.borzenko.dayimpression.storage.SharedPreferencesStorage;
import tech.borzenko.dayimpression.storage.Storage;
import tech.borzenko.dayimpression.ui.fragment.AddImpressionFragment;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 26.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class AddImpressionActivity extends AppCompatActivity
        implements AddImpressionFragment.OnDataValidityListener,
        AddImpressionFragment.OnSaveListener {
    private Storage storage;

    private AddImpressionFragment addImpressionFragment;

    private boolean readyToAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_impression);

        initToolbar();

        if (savedInstanceState == null) {
            addImpressionFragment = new AddImpressionFragment();

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.vg_container, addImpressionFragment)
                    .commit();
        } else {
            addImpressionFragment = (AddImpressionFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.vg_container);
        }

        storage = SharedPreferencesStorage.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_impression, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem saveMenuItem = menu.findItem(R.id.action_save);
        if (saveMenuItem != null) {
            saveMenuItem.setVisible(readyToAdd);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();

                return true;
            }

            case R.id.action_save: {
                addImpressionFragment.saveImpression();

                return true;
            }

            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onDataValid(boolean valid) {
        readyToAdd = valid;
        supportInvalidateOptionsMenu();
    }

    @Override
    public void onSaveImpression(Impression impression) {
        storage.addImpression(impression);
        finish();
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.add_impression);
        }
    }
}
