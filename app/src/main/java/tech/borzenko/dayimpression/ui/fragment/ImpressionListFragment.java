package tech.borzenko.dayimpression.ui.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import tech.borzenko.dayimpression.R;
import tech.borzenko.dayimpression.storage.SharedPreferencesStorage;
import tech.borzenko.dayimpression.storage.Storage;
import tech.borzenko.dayimpression.ui.adapter.ImpressionAdapter;
import tech.borzenko.dayimpression.ui.util.SpaceItemDecoration;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 26.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class ImpressionListFragment extends Fragment {
    private Storage storage;

    private ImpressionAdapter impressionAdapter;

    public ImpressionListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        storage = SharedPreferencesStorage.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_impression_list, container, false);

        RecyclerView impressionRecyclerView = (RecyclerView) view.findViewById(R.id.rv_impressions);
        impressionRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        int itemMargin = getResources().getDimensionPixelSize(R.dimen.card_margin);
        impressionRecyclerView.addItemDecoration(new SpaceItemDecoration(itemMargin));

        impressionAdapter = new ImpressionAdapter(storage.getImpressions());
        impressionRecyclerView.setAdapter(impressionAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        impressionAdapter.setImpressions(storage.getImpressions());
    }
}
