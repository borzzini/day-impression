package tech.borzenko.dayimpression.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import tech.borzenko.dayimpression.R;
import tech.borzenko.dayimpression.ui.fragment.ImpressionListFragment;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 26.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class ImpressionListActivity extends AppCompatActivity
        implements Toolbar.OnMenuItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_impression_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.impression_list);
        toolbar.setOnMenuItemClickListener(this);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.vg_container, new ImpressionListFragment())
                    .commit();
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add: {
                Intent addImpressionIntent = new Intent(this, AddImpressionActivity.class);
                startActivity(addImpressionIntent);

                return true;
            }

            default: {
                return false;
            }
        }
    }
}
