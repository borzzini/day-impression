package tech.borzenko.dayimpression.storage;

import java.util.List;

import tech.borzenko.dayimpression.model.Impression;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 27.02.16
 *
 * @author Stanislav S. Borzenko
 */
public interface Storage {
    List<Impression> getImpressions();

    void addImpression(Impression impression);
}
