package tech.borzenko.dayimpression.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;
import java.util.List;

import tech.borzenko.dayimpression.model.Impression;
import tech.borzenko.dayimpression.storage.serialization.StringImpressionSerializer;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 27.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class SharedPreferencesStorage implements Storage {
    private static final String STORAGE_FILENAME = "storage.pref";

    private static final String KEY_IMPRESSION_LIST = "impression_list";

    private SharedPreferences preferences;
    private StringImpressionSerializer impressionSerializer;

    private SharedPreferencesStorage() {
    }

    public static SharedPreferencesStorage getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public void init(Context context, StringImpressionSerializer impressionSerializer) {
        preferences = context.getSharedPreferences(STORAGE_FILENAME, Context.MODE_PRIVATE);

        this.impressionSerializer = impressionSerializer;
    }

    @Override
    public List<Impression> getImpressions() {
        String serializedImpressionList = preferences.getString(KEY_IMPRESSION_LIST, null);
        if (serializedImpressionList != null) {
            return impressionSerializer.deserializeList(serializedImpressionList);
        }

        return null;
    }

    @Override
    public void addImpression(Impression impression) {
        List<Impression> impressions = getImpressions();
        if (impressions == null) {
            impressions = new ArrayList<>();
        }

        impressions.add(0, impression);

        preferences.edit()
                .putString(KEY_IMPRESSION_LIST, impressionSerializer.serialize(impressions))
                .apply();
    }

    private static class InstanceHolder {
        private static final SharedPreferencesStorage INSTANCE = new SharedPreferencesStorage();
    }
}
