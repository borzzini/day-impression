package tech.borzenko.dayimpression.storage.serialization;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 27.02.16
 *
 * @author Stanislav S. Borzenko
 */
public interface StringImpressionSerializer extends ImpressionSerializer<String> {
}
