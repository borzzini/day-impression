package tech.borzenko.dayimpression.storage.serialization;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import tech.borzenko.dayimpression.model.Impression;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 27.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class JsonImpressionSerializer implements StringImpressionSerializer {
    private Gson gson;

    public JsonImpressionSerializer() {
        gson = new Gson();
    }

    @Override
    public String serialize(Impression impression) {
        return gson.toJson(impression);
    }

    @Override
    public String serialize(List<Impression> impressionList) {
        return gson.toJson(impressionList);
    }

    @Override
    public Impression deserializeItem(String serializedImpression) {
        return gson.fromJson(serializedImpression, Impression.class);
    }

    @Override
    public List<Impression> deserializeList(String serializedImpressionList) {
        Type impressionListType = new TypeToken<List<Impression>>() {
        }.getType();
        return gson.fromJson(serializedImpressionList, impressionListType);
    }
}
