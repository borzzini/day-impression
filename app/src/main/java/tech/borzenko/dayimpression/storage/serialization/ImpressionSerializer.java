package tech.borzenko.dayimpression.storage.serialization;

import java.util.List;

import tech.borzenko.dayimpression.model.Impression;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 27.02.16
 *
 * @author Stanislav S. Borzenko
 */
public interface ImpressionSerializer<T> {
    T serialize(Impression impression);

    T serialize(List<Impression> impressionList);

    Impression deserializeItem(T serializedImpression);

    List<Impression> deserializeList(T serializedImpressionList);
}
