package tech.borzenko.dayimpression;

import android.app.Application;

import java.util.Date;
import java.util.List;

import tech.borzenko.dayimpression.model.Impression;
import tech.borzenko.dayimpression.storage.SharedPreferencesStorage;
import tech.borzenko.dayimpression.storage.serialization.JsonImpressionSerializer;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 27.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class DayImpressionApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        SharedPreferencesStorage.getInstance().init(getApplicationContext(),
                new JsonImpressionSerializer());

        populateImpressionStorageWithSampleData();
    }

    private void populateImpressionStorageWithSampleData() {
        List<Impression> impressions = SharedPreferencesStorage.getInstance().getImpressions();
        if (impressions != null) {
            return;
        }

        SharedPreferencesStorage.getInstance().addImpression(
                new Impression(new Date(1456506644152L),
                        "Solving Katas at codewars.com"));

        SharedPreferencesStorage.getInstance().addImpression(
                new Impression(new Date(1455645600000L),
                        "Discussion about some aspect of working process with a colleague"));

        SharedPreferencesStorage.getInstance().addImpression(
                new Impression(new Date(1455559200000L),
                        "Deep learning of a new corporate document"));

        SharedPreferencesStorage.getInstance().addImpression(
                new Impression(new Date(1454954400000L),
                        "Working on improving of some document"));

        SharedPreferencesStorage.getInstance().addImpression(
                new Impression(new Date(1454608800000L),
                        "English speaking club with girls from Poland and France"));
    }
}
