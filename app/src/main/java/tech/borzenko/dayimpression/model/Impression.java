package tech.borzenko.dayimpression.model;

import java.util.Date;

/**
 * Created at Borzenko Tech (http://borzenko.tech) on 26.02.16
 *
 * @author Stanislav S. Borzenko
 */
public class Impression {
    private Date date;
    private String text;

    public Impression(Date date, String text) {
        this.date = date;
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public String getText() {
        return text;
    }
}
